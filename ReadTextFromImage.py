import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import pytesseract as tess
import cv2
from PIL import Image, ImageEnhance

tess.pytesseract.tesseract_cmd = r'C:\Users\supriya.malhotra\AppData\Local\Tesseract-OCR\tesseract.exe'


print("---------------------------------------------------------------------------------------")

imageName = "SupriyaMalhotraPassport.png"


def increaseImageSharpness(imageName):
    im = Image.open(imageName)
    enhancer = ImageEnhance.Sharpness(im)
    enhanced_im = enhancer.enhance(10.0)
    return enhanced_im.save("enhanced.SupriyaMalhotraPassport.png")


def constrastImage(enhanced_im):
    im = Image.open(imageName)
    enhancer = ImageEnhance.Contrast(im)
    enhanced_im = enhancer.enhance(2.8)
    return enhanced_im.save("constrast.SupriyaMalhotraPassport.png")


#thresholding
def thresholding(constrast_im):
    return cv2.threshold(enhanced_im, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


#Read the scanned passport and print the text
def readImage():
    img = Image.open('enhanced.SupriyaMalhotraPassport.png')
    text = tess.image_to_string(img)
    print(text)


enhanced_im = increaseImageSharpness(imageName)
constrast_im = constrastImage(enhanced_im)
increased_threshold = thresholding(constrast_im)
readImage()




# todo- To run the fill execute the command py .\ReadTextFromImage.py

# todo-collect user input on which document they want to verify
# https://studyfied.com/tutorial/tkinter/optionmenu-widget/
# def change():
#     print("")
#
# root = tk.Tk()
# OPTIONS=[
#     "Passport",
#     "National Identity Card",
#     "Driving Licences",
#     "Biometric Residence Permits/Card",
#     "Military Identity Card",
# ]
#
# var = tk.StringVar(root)
# var.set(OPTIONS[-1])
# var.trace("w",change)
#
# dropDownMenu = tk.OptionMenu(root,var, OPTIONS[-1],OPTIONS[1],OPTIONS[2],OPTIONS[3],OPTIONS[4])
# dropDownMenu.pack()
#
# root.mainloop()
# print("End program")


# todo
# tk.messagebox.showinfo('Window Title','Please select the file path')
#
# root = tk.Tk()
# root.withdraw()
#
# file_path = filedialog.askopenfilename()
#
# print(file_path)
#
# input('Press any key to exit')


# imageName = "SupriyaMalhotraPassport.png"
#
#
# def increaseImageSharpness(imageName):
#     im = Image.open(imageName)
#     enhancer = ImageEnhance.Sharpness(im)
#     enhanced_im = enhancer.enhance(10.0)
#     return enhanced_im.save("enhanced.SupriyaMalhotraPassport.png")
#
#
# # #thresholding
# def thresholding(enhanced_im):
#     return cv2.threshold(enhanced_im, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
#
#
# # #Read the scanned passport and print the text
# def readImage():
#     img = Image.open('enhanced.SupriyaMalhotraPassport.png')
#     text = tess.image_to_string(img)
#     print(text)
#
#
# enhanced_im=increaseImageSharpness(imageName)
# increased_threshold = thresholding(enhanced_im)
# readImage()